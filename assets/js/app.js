

// 1. get the input with id new-task and assign it to taskInput;
var taskInput; 
var addButton = document.getElementsByTagName("button")[0];
var incompleteTasksHolder = document.getElementById('incomplete-tasks'); 


var createNewTaskElement = function(taskString) {
  var listItem = document.createElement('li');
  var label = document.createElement('label');
  var editInput = document.createElement('input');
  var deleteButton = document.createElement('button');
  deleteButton.innerText = 'Delete';
  deleteButton.className = 'delete';
  label.innerText = taskString;
  listItem.appendChild(label);
  listItem.appendChild(deleteButton);

  return listItem;
};

//Add a new task
var addTask = function() {
  var listItem = createNewTaskElement(taskInput.value);
  // 3. Append the list item to incompleteTasksHolder;
  bindTaskEvents(listItem);
  taskInput.value = '';
};



var deleteTask = function() {
  //Remove the parent list item from the ul
  var listItem = this.parentNode;
  var ul = listItem.parentNode;

  ul.removeChild(listItem);
};




// 2.  Set the click handler to the addTask function
addButton;

var bindTaskEvents = function(taskListItem) {
  // select listitems chidlren
  var deleteButton = taskListItem.querySelector('button.delete');
  // 3. Assign deleteTask function to deletButton onclick handler
  deleteButton.onclick;
};
